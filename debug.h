#ifndef DEBUG_H
#define DEBUG_H

#define DEBUG 

#ifdef DEBUG
# define bugprint(...) fprintf(stderr, "%s %d: ", __FILE__,__LINE__);fprintf(stderr, __VA_ARGS__)
#else
# define bugprint(...)
#endif

#endif
