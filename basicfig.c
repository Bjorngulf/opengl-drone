#include "basicfig.h"


void drawClosedCyl(GLenum mode, GLdouble rbasis, GLdouble rtop, GLdouble hoogte, GLint q)
{
    /* 
     * Function for drawing a cylinder with closed top and bottom
     */

    if(mode == GL_FILL){
        mode = GLU_FILL;
    }
    else{
        mode = GLU_LINE;
    }

    glPushMatrix();
    /* Cylinder */
    GLUquadricObj *p = gluNewQuadric();
    gluQuadricDrawStyle(p, mode);
    gluCylinder(p, rbasis, rtop, hoogte, q, q);
    gluDeleteQuadric(p);

    /* bottom disk */
    p = gluNewQuadric();
    gluQuadricDrawStyle(p, mode);
    gluDisk(p, 0, rbasis, q, q);
    gluDeleteQuadric(p);

    /* top disk */
    glTranslatef(0, 0, hoogte);
    p = gluNewQuadric();
    gluQuadricDrawStyle(p, mode);
    gluDisk(p, 0, rtop, q, q);
    gluDeleteQuadric(p);

    glPopMatrix();
}

