#-*-makefile-*-

TARGETS = drone.out

all All: $(TARGETS)

################################################################################
INCPATH = -I/usr/local/include
LIBPATH = -L. -L/usr/local/lib/hpux32
#LIBS = -lglut -lGLU -lGL -lGLw -lMrm -lXm -lXt -lXext -lX11 -ljpeg -lm 
LIBS = -lglut -lGLU -lGL -lm -ljpeg
DEBUGFLAGS = -O3 -Wall
CFLAGS = -DD32 $(DEBUGFLAGS) $(INCPATH)


SRCS = materials.c basicfig.c drawdrone.c InitJPG.c drone.c
OBJS = $(SRCS:.c=.o)

.SUFFIXES:	.c

.c.o:	
	cc -c $< $(CFLAGS)


$(TARGETS):  $(OBJS)
	LDOPTS= \
	cc -o $(TARGETS) $(CFLAGS) $(OBJS) $(LIBPATH) $(LIBS) 

clean:
	rm -f $(TARGETS) $(OBJS) core

start: $(TARGETS)
	@echo ------------
	@./$(TARGETS)
	@echo ------------
	
