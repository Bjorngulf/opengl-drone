#include "materials.h"

struct Materiaal * initMaterials(const int visual, int * aant)
{
	/*
	 * Maakt een array van structs aan.
     * En leest de data van de verschillende materialen binnen.
	 * Vergeet de teruggegeven pointer niet vrij te geven na gebruik
     * param: visual: 0 niet afprinten
     *                1 afprinten
	 */
	 
    FILE *fp;
    int aantal;
    int i;
    struct Materiaal *materials;

    fp = fopen(DATA, "r");
    if(!fp){
        fprintf(stderr, "Error bij het openen van bestand.\n");
        exit(1);
    }

    fscanf(fp, "%d ; \n", &aantal);
    materials = calloc(aantal, sizeof(struct Materiaal));

    fprintf(stdout, "| %15s | %23s | %23s | %23s | %10s |\n", "Naam", "Ambient", "Diffuus",
           "Specular", "Shininess");
    fprintf(stdout, "----------------------\n");

    for(i=0; i<aantal; i++){
        fscanf(fp, "%s ; ", materials[i].name);
        fscanf(fp, "%f ; %f ; %f ; %f ; ", &materials[i].ambient[0], 
                &materials[i].ambient[1], &materials[i].ambient[2], 
                &materials[i].ambient[3]);
        fscanf(fp, "%f ; %f ; %f ; %f ; ", &materials[i].diffuus[0], 
                &materials[i].diffuus[1], &materials[i].diffuus[2], 
                &materials[i].diffuus[3]);
        fscanf(fp, "%f ; %f ; %f ; %f ; ", &materials[i].specular[0], 
                &materials[i].specular[1], &materials[i].specular[2], 
                &materials[i].specular[3]);
        fscanf(fp, "%f ; \n", &materials[i].shininess);

		if(visual){
			fprintf(stdout, "| %15s | ", materials[i].name);
			fprintf(stdout, "%5.2f %5.2f %5.2f %5.2f | ", materials[i].ambient[0], 
					materials[i].ambient[1], materials[i].ambient[2], 
                    materials[i].ambient[3]);
			fprintf(stdout, "%5.2f %5.2f %5.2f %5.2f | ", materials[i].diffuus[0], 
					materials[i].diffuus[1], materials[i].diffuus[2], 
                    materials[i].diffuus[3]);
			fprintf(stdout, "%5.2f %5.2f %5.2f %5.2f | ", materials[i].specular[0], 
					materials[i].specular[1], materials[i].specular[2], 
					materials[i].specular[3]);
			fprintf(stdout, "%10.2f |\n", materials[i].shininess);
		}
    }
    
    *aant = aantal;
    fclose(fp);
    return materials;
}
