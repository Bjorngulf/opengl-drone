#include "drawdrone.h"


void landingsPoot(const int lengte, const float verp, const int hoekLand, GLenum mode)
{
    /*
     * Draw a landingsgear
     */

    glPushMatrix();

    /* Hoek landingsgestel */
    glRotatef(hoekLand, 0, 0, 1);

    /* Landingsgestel */
    glPushMatrix();
        glTranslatef(verp, -verp, 0);
        glRotatef(-45, 0, 0, 1);
        glScalef(lengte, 1, 1);
        if(mode == GL_FILL){
            glutSolidCube(1);
        }
        else{
            glutWireCube(1);
        }
    glPopMatrix();

    glPopMatrix();
}

void propellor(const int hoekProp, GLenum mode, GLint q, int showPoints)
{
    /*
     * Draw a propellor
     */
    const double posProp[4][4] = {{-1.25, 0.5, -10, 0}, {-1.25, 0.5, 1, 0}, 
                                 {1, 0.5, 1.25, 90}, {-10, 0.5, 1.25, 90}};
    int i;

    glPushMatrix();
   
    /* rotate propellor */
    glRotatef(hoekProp, 0, 1, 0);


    /* Draw cylinder */
    glPushMatrix();
        glTranslatef(0, 1, 0);
        glRotatef(90, 1, 0, 0);
        drawClosedCyl(mode, 1.25, 1.25, 1, q);
    glPopMatrix();

    /* Draw Propellor */
    for(i=0; i<4; i++){
    glPushMatrix();
        glTranslatef(posProp[i][0], posProp[i][1], posProp[i][2]);
        glRotatef(posProp[i][3], 0, 1, 0);
        bezier(mode, showPoints);
    glPopMatrix();
    }

    glPopMatrix();
}

void bezier(GLenum mode, int showPoints)
{
    /* Points for propellor bezier */
    const GLfloat pointsProp[4][4][3] = {
        { {0, 0, 0}, {1, 0.5, 0}, {1.5,0.5,0}, {2.5,0, 0} },
        { {0, 0, 3}, {1, 0.5, 3}, {1.5,0.5,3}, {2.5,0, 3} },
        { {0, 0, 6}, {1, 0.5, 6}, {1.5,0.5,6}, {2.5,0, 6} },
        { {0, 0, 9}, {1, 0.5, 9}, {1.5,0.5,9}, {2.5,0, 9} },
    };
    int i, j;

    glPushMatrix();
    /* normalize for lights */
    glNormal3f(0, 1, 0);
       
    /* Topside wing */
    glPushMatrix();
        glMap2f(GL_MAP2_VERTEX_3, 0, 1, 3, 4, 0, 1, 12, 4, &pointsProp[0][0][0]);
        glEnable(GL_MAP2_VERTEX_3);
        glMapGrid2f(20, 0.0, 1.0, 20, 0.0, 1.0);
        glEvalMesh2(mode, 0, 20, 0, 20);
        glDisable(GL_MAP2_VERTEX_3);
    glPopMatrix();

    /* Bottom wing */
    glPushMatrix();
        glTranslatef(2.5, 0, 0);
        glRotatef(180, 0, 0, 1);
        glMap2f(GL_MAP2_VERTEX_3, 0, 1, 3, 4, 0, 1, 12, 4, &pointsProp[0][0][0]);
        glEnable(GL_MAP2_VERTEX_3);
        glMapGrid2f(20, 0.0, 1.0, 20, 0.0, 1.0);
        glEvalMesh2(mode, 0, 20, 0, 20);
        glDisable(GL_MAP2_VERTEX_3);
    glPopMatrix();

    if(showPoints){
        glPointSize(7.0);
        glDisable(GL_LIGHTING);
        glColor3f(1.0, 1.0, 0.0 );
        glBegin(GL_POINTS);
            for (i = 0; i < 4; i++) {
                for (j = 0; j < 4; j++) {
                    glVertex3f(pointsProp[i][j][0], 
                    pointsProp[i][j][1], pointsProp[i][j][2]);
                }
            }
        glEnd();
        glEnable(GL_LIGHTING);
    }


    glPopMatrix();
}

void bSpline(GLenum mode, int showPoints)
{
    /* Controlpoints for B-Spline */
    GLfloat ctlpoints[4][4][3] = {
        { {0, 0, 0}, {7, 10, 0}, {14, 10, 0}, {20, 0, 0} },
        { {2, 0, 4}, {8, 7, 4}, {12, 7, 4}, {18, 0, 4} },
        { {4, 0, 8}, {10, 5, 8}, {10, 5, 8}, {16, 0, 8} },
        { {9, 0, 10}, {10, 0, 10}, {10, 0, 10}, {11, 0, 10} },
    };
    /* Knots for B-Spline */
    GLfloat knots[8] = {0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0};
    int i, j;

    /* Normalize for lights */
    glNormal3f(0, 1, 0);

    /* Wire frame?? */
    if(mode == GL_FILL){
        mode = GLU_FILL;
    }
    else{
        mode = GLU_OUTLINE_POLYGON;
    }

    glPushMatrix();
    glTranslatef(-10, 0, 0);
    glRotatef(90, 1, 0, 0);
    
    GLUnurbsObj *theNurb = gluNewNurbsRenderer();
    gluNurbsProperty(theNurb, GLU_SAMPLING_TOLERANCE, 25.0);
    gluNurbsProperty(theNurb, GLU_DISPLAY_MODE, mode);
    gluBeginSurface(theNurb); gluNurbsSurface(theNurb, 8, knots, 8, knots, 4 *
            3, 3, &ctlpoints[0][0][0], 4, 4, GL_MAP2_VERTEX_3);
    gluEndSurface(theNurb);
    gluDeleteNurbsRenderer(theNurb);

    if(showPoints){
        glPointSize(7.0);
        glDisable(GL_LIGHTING);
        glColor3f(1.0, 1.0, 0.0 );
        glBegin(GL_POINTS);
            for (i = 0; i < 4; i++) {
                for (j = 0; j < 4; j++) {
                    glVertex3f(ctlpoints[i][j][0], 
                    ctlpoints[i][j][1], ctlpoints[i][j][2]);
                }
            }
        glEnd();
        glEnable(GL_LIGHTING);
    }

    glPopMatrix();
}

void lichaam(GLenum mode, int showPoints)
{
    /*
     * Draws body of two B-Splines around cone
     */

    glPushMatrix();

    glPushMatrix();
        bSpline(mode, showPoints);
    glPopMatrix();

    glPushMatrix();
        glRotatef(180, 0,1,0);
        bSpline(mode, showPoints);
    glPopMatrix();


    glPopMatrix();
}

void frame(GLenum mode, GLint q)
{
    /*
     * Draws the whole frame
     */

    const int posCyl[4][4] = {{0, 5, -25, 90}, {-25, 5, 0, 90}, 
                             {25, 5, 0, 90}, {0, 5, 25, 90}};
    int i;
    

	glPushMatrix();
	
	/* Stangen */
    glPushMatrix();
        glScalef(50, 4, 4);    
        if(mode == GL_FILL){  
            glutSolidCube(1);
        }
        else{
            glutWireCube(1);
        }
    glPopMatrix();

    glPushMatrix();
        glRotatef(90, 0, 1, 0);
        glScalef(50, 4, 4);
        if(mode == GL_FILL){  
            glutSolidCube(1);
        }
        else{
            glutWireCube(1);
        }
    glPopMatrix();
    
    /* Torus */
    glPushMatrix();
        glRotatef(90, 1, 0, 0);
        if(mode == GL_FILL){
            glutSolidTorus(2, 10, 10, q);
        }
        else{
            glutWireTorus(2, 10, 10, q);
        }
    glPopMatrix();

    /* cylinders */
    for(i=0; i<4; i++){
        glPushMatrix();
            glTranslatef(posCyl[i][0], posCyl[i][1], posCyl[i][2]);
            glRotatef(posCyl[i][3], 1, 0, 0);
            drawClosedCyl(mode, 2, 2, 7, q);
        glPopMatrix();
    }
    
    
    /* Kegel */
    glPushMatrix();
        glTranslatef(0, -2, 0);
        glRotatef(90, 1, 0, 0);
        drawClosedCyl(mode, 2, 1, 3, q);
    glPopMatrix();
    
    glPopMatrix();
}
